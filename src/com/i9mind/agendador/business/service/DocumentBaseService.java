package com.i9mind.agendador.business.service;

import java.util.List;

import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.i9mind.agendador.comum.exception.IntegrationException;

public interface DocumentBaseService {
	
	public List<?> listar(DocumentBase pDocumentBase) throws IntegrationException;
	
	DocumentBase obter(DocumentBase pDocumentBase) throws IntegrationException;
	
	public String inserir(DocumentBase pDocumentBase) throws IntegrationException;
	
	public void atualizar(DocumentBase pDocumentBase) throws IntegrationException;
	
	void excluir(DocumentBase pDocumentBase) throws IntegrationException;
	
	public void inativar(DocumentBase pDocumentBase) throws IntegrationException;
	
	void ativar(DocumentBase pDocumentBase ) throws IntegrationException;
	
	List<?> listarInativo(DocumentBase pDocumentBase) throws IntegrationException;    

}