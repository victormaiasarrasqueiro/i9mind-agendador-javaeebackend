package com.i9mind.agendador.business.service.impl;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.i9mind.agendador.business.entity.Cliente;
import com.i9mind.agendador.business.entity.EmpresaConveniada;
import com.i9mind.agendador.business.entity.Especialidade;
import com.i9mind.agendador.business.entity.OperadoraSaude;
import com.i9mind.agendador.business.entity.Profissional;
import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.i9mind.agendador.business.repository.mongo.ClienteMongoRepository;
import com.i9mind.agendador.business.repository.mongo.DocumentBaseMongoRepository;
import com.i9mind.agendador.business.repository.mongo.EmpresaConveniadaMongoRepository;
import com.i9mind.agendador.business.repository.mongo.EspecialidadeMongoRepository;
import com.i9mind.agendador.business.repository.mongo.OperadoraSaudeMongoRepository;
import com.i9mind.agendador.business.repository.mongo.ProfissionalMongoRepository;
import com.i9mind.agendador.comum.exception.IntegrationException;
import com.i9mind.agendador.integration.service.rest.controller.BaseRestController;

@Component
public class BaseService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseRestController.class);
	
	@Autowired
	EspecialidadeMongoRepository especialidadeMongoRepository;
	
	@Autowired
	OperadoraSaudeMongoRepository operadorasaudeMongoRepository;
	
	@Autowired
	ClienteMongoRepository clienteMongoRepository;
	
	@Autowired
	ProfissionalMongoRepository profissionalMongoRepository;
	
	@Autowired
	EmpresaConveniadaMongoRepository empresaConveniadaMongoRepository;
	
	/**
	 * 
	 * Recupera o objeto.
	 * 
	 * @return DocumentBase - DocumentBase
	 * 
	 */
	public DocumentBaseMongoRepository getRepository(DocumentBase p){
		
		try
		{
			
			if(p instanceof Especialidade)
			{
				return especialidadeMongoRepository;
			}
			else if(p instanceof OperadoraSaude)
			{
				return operadorasaudeMongoRepository;
			}
			else if(p instanceof Cliente)
			{
				return clienteMongoRepository;
			}
			else if(p instanceof Profissional)
			{
				return profissionalMongoRepository;
			}
			else if(p instanceof EmpresaConveniada)
			{
				return empresaConveniadaMongoRepository;
			}
			else
			{
				return null;
			}

		}
		catch(Exception e)
    	{
			LOGGER.error("BaseGenericoService - DocumentBaseMongoRepository",e);
			throw new IntegrationException("EspecialidadeServiceImpl", "listar" , "Erro ao listar", e);
		}
		
	};
	
	/**
	 * 
	 * Retorna o ID da Empresa logada.
	 * 
	 * @return String
	 * 
	 */
	public String getIDEmpresa(){
		
		try
		{
			return "1";
		}
    	catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "getIDEmpresa" , "Erro ao recuperar o ID da Empresa Logada.", e);
		}	
		
	};
	
	/**
	 * 
	 * Retorna o ID da usuario logado..
	 * 
	 * @return String
	 * 
	 */
	public BigInteger getIDUsuarioCadastro(){
		
		try
		{
			return new BigInteger("1");
		}
    	catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "getIDUsuarioCadastro" , "Erro ao recuperar o ID do usuario Logado.", e);
		}	
		
	}

	
	public EspecialidadeMongoRepository getEspecialidadeMongoRepository() {
		return especialidadeMongoRepository;
	}

	public OperadoraSaudeMongoRepository getOperadorasaudeMongoRepository() {
		return operadorasaudeMongoRepository;
	}

	public ClienteMongoRepository getClienteMongoRepository() {
		return clienteMongoRepository;
	}

};