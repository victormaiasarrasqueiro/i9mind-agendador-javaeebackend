package com.i9mind.agendador.business.document;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */

@Document(collection="clientes")
@JsonInclude(Include.NON_NULL)
public class ClienteDoc implements Serializable  {

	private static final long serialVersionUID = -8192132802523770929L;

	@Id
	@JsonProperty(value = "id")
	private BigInteger id;
	
	@Indexed
	@Field("idSql")
	@JsonProperty(value = "idSql")
	private BigInteger idSql;
	
	@Indexed
	@Field("idCli")
	@JsonProperty(value = "idCli")
	private BigInteger idClinica;

	@TextIndexed
	@Field("nmPri")
	@NotNull
	@Size(min=2, max=15)
	@JsonProperty(value = "nmPri")
	private String nomePrimeiro;     
    
	@TextIndexed
	@Field("nmUlt")
	@NotNull
	@Size(min=2, max=40)
	@JsonProperty(value = "nmUlt")
	private String nomeUltimo;
    
	@TextIndexed
	@Field("nuCel")
	@Size(min=8, max=18)
	@JsonProperty(value = "nuCel")
	private String numeroCelular;
	
	@TextIndexed
	@Field("nuIde")
	@Size(min=8, max=11)
	@JsonProperty(value = "nuIde")
	private String numeroIdentidade;    
	
	@Field("dtNas")
	@Past
	@NotNull
	@JsonProperty(value = "dtNas")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Date dataNascimento;        
	
	@Field("tpSex")
	@DecimalMin(value = "0")
	@DecimalMax(value = "2")
	@JsonProperty(value = "tpSex")
	private Byte tipoSexo;       		// Sexo  ( 0 - Não Informado   1 - Masculino   2 - Feminino  )

	@Field("tpECi")
	@DecimalMin(value = "0")
	@DecimalMax(value = "6")
	@JsonProperty(value = "tpECi")
	private Byte tipoEstadoCivil;    	// Estado Civil ( 0 - Não Informado  1 - Solteiro(a) 2 - Casado(a) 3 - Divorciado(a) 4 -Viúvo(a)  5 - Separado(a)  6 - Companheiro(a) )  

	@Field("isRes")
	@NotNull
	@JsonProperty(value = "isRes")
	private Boolean isResponsavel;   	// Responsável ( TRUE - SIM, é seu proprio responsavel    FALSE- NÂO, POSSUI RESPONSAVEL  )

	@Field("isDef")
	@NotNull
	@JsonProperty(value = "isDef")
	private Boolean isDeficiente;   	// Deficiência ( TRUE - POSSUI    FALSE- NÂO POSSUI  )

	@Field("nuCpf")
	@Size(min=8, max=13)
	@JsonProperty(value = "nuCpf")
	private String numeroCpf;    		// Numero do CPF do Cliente
	
	@Field("tpCon")
	@DecimalMin(value = "0")
	@DecimalMax(value = "3")
	@JsonProperty(value = "tpCon")
	private Byte tipoConvenio;      	// Tipo de Convênio  ( 0 - Não Informado   1 - Operadora de Saude   2 - Empresas   3 - Outros  )  

	@Field("nuTel")
	@Size(min=7, max=18)
	@JsonProperty(value = "nuTel")
	private String numeroTelefone;

	@Field("nuWap")
	@Size(min=8, max=18)
	@JsonProperty(value = "nuWap")
	private String numeroWhatsapp;

	@Field("dsEma")
	@Size(min=8, max=40)
	@JsonProperty(value = "dsEma")
	private String email;

	@Field("nuCep")
	@Size(min=5, max=9)
	@JsonProperty(value = "nuCep")
	private String numeroCEP;
	
	@Field("dsLog")
	@Size(min=3, max=40)
	@JsonProperty(value = "dsLog")
	private String logradouro;

	@Field("nuLog")
	@Size(min=1, max=10)
	@JsonProperty(value = "nuLog")
	private String numeroLogradouro;

	@Field("dsBai")
	@Size(min=2, max=20)
	@JsonProperty(value = "dsBai")
	private String bairro;

	@Field("dsCid")
	@Size(min=2, max=20)
	@JsonProperty(value = "dsCid")
	private String cidade;

	@Field("dsEst")
	@Size(min=2, max=20)
	@JsonProperty(value = "dsEst")
	private String estado;

	@Field("dsPai")
	@Size(min=2, max=20)
	@JsonProperty(value = "dsPai")
	private String pais;

	@JsonIgnore
	private Boolean ativo;
	
	
	
	
	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getNomePrimeiro() {
		return nomePrimeiro;
	}

	public void setNomePrimeiro(String nomePrimeiro) {
		this.nomePrimeiro = nomePrimeiro;
	}

	public String getNomeUltimo() {
		return nomeUltimo;
	}

	public void setNomeUltimo(String nomeUltimo) {
		this.nomeUltimo = nomeUltimo;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNumeroIdentidade() {
		return numeroIdentidade;
	}

	public void setNumeroIdentidade(String numeroIdentidade) {
		this.numeroIdentidade = numeroIdentidade;
	}

	public Byte getTipoConvenio() {
		return tipoConvenio;
	}

	public void setTipoConvenio(Byte tipoConvenio) {
		this.tipoConvenio = tipoConvenio;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

