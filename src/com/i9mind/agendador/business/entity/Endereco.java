package com.i9mind.agendador.business.entity;

import java.io.Serializable;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@JsonInclude(Include.NON_NULL)
public class Endereco implements Serializable{

	private static final long serialVersionUID = 2346519996274210748L;

	@Size(min=5, max=9)
	@JsonProperty(value = "nuCep")
	private String numeroCEP;
	
	@Size(min=3, max=40)
	@JsonProperty(value = "dsLog")
	private String logradouro;

	@Size(min=1, max=10)
	@JsonProperty(value = "nuLog")
	private String numeroLogradouro;
	
	@Size(min=1, max=70)
	@JsonProperty(value = "dsLoC")
	private String complementoLogradouro;

	@Size(min=2, max=30)
	@JsonProperty(value = "dsBai")
	private String bairro;

	@Size(min=2, max=30)
	@JsonProperty(value = "dsCid")
	private String cidade;

	@Size(min=2, max=30)
	@JsonProperty(value = "dsEst")
	private String estado;

	@Size(min=2, max=20)
	@JsonProperty(value = "dsPai")
	private String pais;

	
	
	/**
	 *
	 * Validate
	 * 
	 */
	
	public boolean validate(){
		
		return true;
		
	}
	
	public String getNumeroCEP() {
		return numeroCEP;
	}

	public void setNumeroCEP(String numeroCEP) {
		this.numeroCEP = numeroCEP;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumeroLogradouro() {
		return numeroLogradouro;
	}

	public void setNumeroLogradouro(String numeroLogradouro) {
		this.numeroLogradouro = numeroLogradouro;
	}

	public String getComplementoLogradouro() {
		return complementoLogradouro;
	}

	public void setComplementoLogradouro(String complementoLogradouro) {
		this.complementoLogradouro = complementoLogradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

}

