package com.i9mind.agendador.business.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@Document(collection="empresaconveniada")
@JsonInclude(Include.NON_NULL)
public class EmpresaConveniada extends DocumentBase implements Serializable  {

	private static final long serialVersionUID = -4353521062526494054L;

	@Id
	@JsonProperty(value = "id")
	private String id;

	@Indexed
	@Field("idEmp")
	@JsonIgnore
	private String idEmpresa;

	@TextIndexed
	@Field("nmEmp")
	@NotNull
	@Size(min=3, max=30)
	@JsonProperty(value = "nmEmp")
	private String nomeEmpresaConveniada;

	@Field("idUcd")
	@JsonProperty(value = "idUcd")
	private BigInteger idUsuarioCadadastro;
	
	@Field("dtCad")
	@JsonProperty(value = "dtCad")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Date dataCadastroSistema;      
	
	@Field("ieAtv")
	@JsonProperty(value = "ieAtv")
	private Boolean ativo;

	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNomeEmpresaConveniada() {
		return nomeEmpresaConveniada;
	}

	public void setNomeEmpresaConveniada(String nomeEmpresaConveniada) {
		this.nomeEmpresaConveniada = nomeEmpresaConveniada;
	}

	public BigInteger getIdUsuarioCadadastro() {
		return idUsuarioCadadastro;
	}

	public void setIdUsuarioCadadastro(BigInteger idUsuarioCadadastro) {
		this.idUsuarioCadadastro = idUsuarioCadadastro;
	}

	public Date getDataCadastroSistema() {
		return dataCadastroSistema;
	}

	public void setDataCadastroSistema(Date dataCadastroSistema) {
		this.dataCadastroSistema = dataCadastroSistema;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}


}

