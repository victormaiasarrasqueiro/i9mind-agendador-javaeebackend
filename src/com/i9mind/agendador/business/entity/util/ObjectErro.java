package com.i9mind.agendador.business.entity.util;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@JsonInclude(Include.NON_NULL)
public class ObjectErro implements Serializable  {

	private static final long serialVersionUID = -840910782057070970L;

	@JsonProperty(value = "id")
	private Integer id;
	
	@JsonProperty(value = "ds")
	
	private String ds;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDs() {
		return ds;
	}

	public void setDs(String ds) {
		this.ds = ds;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

