package com.i9mind.agendador.business.entity.util;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@JsonInclude(Include.NON_NULL)
public class HttpResponse implements Serializable  {

	private static final long serialVersionUID = 6457104380312575841L;

	@JsonProperty(value = "errors")
	private List<ObjectErro> errors;
	
	@JsonProperty(value = "object")
	private Object object;
	
	public HttpResponse(Object object) {
		super();
		this.errors = null;
		this.object = object;
	}
	
	public HttpResponse(Object object, List<ObjectErro> errors) {
		super();
		this.errors = errors;
		this.object = object;
	}

	public List<ObjectErro> getErrors() {
		return errors;
	}

	public void setErrors(List<ObjectErro> errors) {
		this.errors = errors;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

};

