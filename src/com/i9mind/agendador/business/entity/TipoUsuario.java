package com.i9mind.agendador.business.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@Entity
@JsonInclude(Include.NON_NULL)
public class TipoUsuario implements Serializable  {

	private static final long serialVersionUID = 3193804691544970930L;
	
	@Id
	@JsonProperty(value = "id")
	private Byte id;
	
	@NotNull
	@Size(min=2, max=10)
	@JsonProperty(value = "tpUsu")
	private String tipoUsuario;

	
	public Byte getId() {
		return id;
	}

	public void setId(Byte id) {
		this.id = id;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

