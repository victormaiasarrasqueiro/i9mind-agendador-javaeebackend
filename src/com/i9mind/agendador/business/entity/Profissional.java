package com.i9mind.agendador.business.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.i9mind.agendador.comum.util.auxiliary.StringUtils;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */

@Document(collection="profissional")
@JsonInclude(Include.NON_NULL)
public class Profissional extends DocumentBase implements Serializable {

	private static final long serialVersionUID = 8184112667603457079L;

	
	@Id
	@JsonProperty(value = "id")
	private String id;

	
	@Indexed
	@Field("idEmp")
	@JsonIgnore
	private String idEmpresa;

	@Field("idUcd")
	@JsonIgnore
	private BigInteger idUsuarioCadadastro;
	
	@Field("dtCad")
	@JsonIgnore
	private Date dataCadastroSistema;      
	
	@Field("ieAtv")
	@JsonIgnore
	private Boolean ativo;
	
	
	//**** FILDS ****//
	
	@TextIndexed
	@Field("nmPro")
	@NotNull
	@Size(min=3, max=60)
	@JsonProperty(value = "nmPro")
	private String nomeProfissional;
	
	@Field("dtNas")
	@Past
	@JsonProperty(value = "dtNas")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
	private Date dataNascimento;        
	
	@Field("tpSex")
	@DecimalMin(value = "0")
	@DecimalMax(value = "2")
	@JsonProperty(value = "tpSex")
	private Byte tipoSexo;       		// Sexo  ( 0 - Não Informado   1 - Masculino   2 - Feminino  )
	

	@Field("objEnd")
	@JsonProperty(value = "objEnd")
	private Endereco endereco;
	
	@Field("objDoc")
	@JsonProperty(value = "objDoc")
	private Documentacao documentacao;
	
	@Field("objCon")
	@JsonProperty(value = "objCon")
	private Contato contato;
	

	
	@Field("tpJtr")
	@Size(min=0, max=2)
	@JsonProperty(value = "tpJtr")
	private String tipoJornadaTrabalho;    // Tipo de Jornada de Trabalho  ( 1 - Tradicional  2- Personalizado  )
	
	
	
	@DBRef
	@Field("ltEsp")
	@JsonProperty(value = "ltEsp")
	private List<Especialidade> listaEspecialidade;
	
	@Field("ltPrd")
	@JsonProperty(value = "ltPrd")
	private List<Periodo> listaPeriodoTrabalho;

	
	
	/**
	 *
	 * Validate
	 * 
	 */
	
	public boolean validate(){
		
		if(StringUtils.isEmpty(nomeProfissional)){
			return false;
		}
		


		return true;
		
	}
	
	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public BigInteger getIdUsuarioCadadastro() {
		return idUsuarioCadadastro;
	}

	public void setIdUsuarioCadadastro(BigInteger idUsuarioCadadastro) {
		this.idUsuarioCadadastro = idUsuarioCadadastro;
	}

	public Date getDataCadastroSistema() {
		return dataCadastroSistema;
	}

	public void setDataCadastroSistema(Date dataCadastroSistema) {
		this.dataCadastroSistema = dataCadastroSistema;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getNomeProfissional() {
		return nomeProfissional;
	}

	public void setNomeProfissional(String nomeProfissional) {
		this.nomeProfissional = nomeProfissional;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Documentacao getDocumentacao() {
		return documentacao;
	}

	public void setDocumentacao(Documentacao documentacao) {
		this.documentacao = documentacao;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public String getTipoJornadaTrabalho() {
		return tipoJornadaTrabalho;
	}

	public void setTipoJornadaTrabalho(String tipoJornadaTrabalho) {
		this.tipoJornadaTrabalho = tipoJornadaTrabalho;
	}

	public List<Especialidade> getListaEspecialidade() {
		return listaEspecialidade;
	}

	public void setListaEspecialidade(List<Especialidade> listaEspecialidade) {
		this.listaEspecialidade = listaEspecialidade;
	}

	public List<Periodo> getListaPeriodoTrabalho() {
		return listaPeriodoTrabalho;
	}

	public void setListaPeriodoTrabalho(List<Periodo> listaPeriodoTrabalho) {
		this.listaPeriodoTrabalho = listaPeriodoTrabalho;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Byte getTipoSexo() {
		return tipoSexo;
	}

	public void setTipoSexo(Byte tipoSexo) {
		this.tipoSexo = tipoSexo;
	}
	
	
}

