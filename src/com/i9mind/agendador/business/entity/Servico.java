package com.i9mind.agendador.business.entity;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */

@Entity
@JsonInclude(Include.NON_NULL)
public class Servico implements Serializable  {

	private static final long serialVersionUID = 5486987099469546646L;

	@Id
	@JsonProperty(value = "id")
	private BigInteger id;
	
	@NotNull
	@JsonProperty(value = "nmSev")
	private String nomeServico;
	
	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}     

}

