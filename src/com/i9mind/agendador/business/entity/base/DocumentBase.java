package com.i9mind.agendador.business.entity.base;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
public class DocumentBase implements Serializable  {

	private static final long serialVersionUID = -6728325477009213097L;


	private String 		id;
	private String   	idEmpresa;
	private BigInteger 	idUsuarioCadadastro;
	private Date 		dataCadastroSistema;      
	private Boolean 	ativo;
	
	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public BigInteger getIdUsuarioCadadastro() {
		return idUsuarioCadadastro;
	}
	public void setIdUsuarioCadadastro(BigInteger idUsuarioCadadastro) {
		this.idUsuarioCadadastro = idUsuarioCadadastro;
	}
	public Date getDataCadastroSistema() {
		return dataCadastroSistema;
	}
	public void setDataCadastroSistema(Date dataCadastroSistema) {
		this.dataCadastroSistema = dataCadastroSistema;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

