package com.i9mind.agendador.business.entity;

import java.io.Serializable;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@JsonInclude(Include.NON_NULL)
public class Documentacao implements Serializable{

	private static final long serialVersionUID = 3642911478973061314L;

	@Size(min=8, max=11)
	@JsonProperty(value = "nuIde")
	private String numeroIdentidade;

	@Size(min=3, max=10)
	@JsonProperty(value = "dsOER")
	private String dsOrgaoEmissor;

	@Size(min=1, max=2)
	@JsonProperty(value = "idOEU")
	private String idOrgaoEmissorUF;       	
	
	@Size(min=8, max=13)
	@JsonProperty(value = "nuCpf")
	private String numeroCpf;    					// Numero do CPF do Cliente

	@DecimalMin(value = "0")
	@DecimalMax(value = "6")
	@JsonProperty(value = "tpECi")
	private Byte tipoEstadoCivil;    				// Estado Civil ( 0 - Não Informado  1 - Solteiro(a)  2 - Casado(a) 3 - Divorciado(a) 4 -Viúvo(a)  5 - Separado(a)  6 - Companheiro(a) )  

	
	
	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	

	public String getNumeroIdentidade() {
		return numeroIdentidade;
	}

	public void setNumeroIdentidade(String numeroIdentidade) {
		this.numeroIdentidade = numeroIdentidade;
	}

	public String getDsOrgaoEmissor() {
		return dsOrgaoEmissor;
	}

	public void setDsOrgaoEmissor(String dsOrgaoEmissor) {
		this.dsOrgaoEmissor = dsOrgaoEmissor;
	}

	public String getIdOrgaoEmissorUF() {
		return idOrgaoEmissorUF;
	}

	public void setIdOrgaoEmissorUF(String idOrgaoEmissorUF) {
		this.idOrgaoEmissorUF = idOrgaoEmissorUF;
	}

	public String getNumeroCpf() {
		return numeroCpf;
	}

	public void setNumeroCpf(String numeroCpf) {
		this.numeroCpf = numeroCpf;
	}

	public Byte getTipoEstadoCivil() {
		return tipoEstadoCivil;
	}

	public void setTipoEstadoCivil(Byte tipoEstadoCivil) {
		this.tipoEstadoCivil = tipoEstadoCivil;
	}

}

