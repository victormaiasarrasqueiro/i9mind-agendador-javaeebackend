package com.i9mind.agendador.business.repository.mongo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.i9mind.agendador.comum.exception.IntegrationException;

@Repository
public interface DocumentBaseMongoRepository{
	
	List<?> listar(DocumentBase pDocumentBase) throws IntegrationException;    
	
	DocumentBase obter(DocumentBase pDocumentBase) throws IntegrationException;
	
	DocumentBase inserir(DocumentBase pDocumentBase) throws IntegrationException;
	
	void atualizar(DocumentBase pDocumentBase) throws IntegrationException;
	
	void excluir(DocumentBase pDocumentBase) throws IntegrationException;
	
	void inativar(DocumentBase pDocumentBase ) throws IntegrationException;
	
	void ativar(DocumentBase pDocumentBase ) throws IntegrationException;
	
	List<?> listarInativo(DocumentBase pDocumentBase) throws IntegrationException;    
	
};
