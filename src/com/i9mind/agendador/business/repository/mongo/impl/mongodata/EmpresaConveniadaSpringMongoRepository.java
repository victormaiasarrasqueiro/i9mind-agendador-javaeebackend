package com.i9mind.agendador.business.repository.mongo.impl.mongodata;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.i9mind.agendador.business.entity.EmpresaConveniada;

@Repository
public interface EmpresaConveniadaSpringMongoRepository extends MongoRepository<EmpresaConveniada, Long> {

}
