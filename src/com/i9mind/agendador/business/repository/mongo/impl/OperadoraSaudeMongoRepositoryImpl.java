package com.i9mind.agendador.business.repository.mongo.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.i9mind.agendador.business.entity.OperadoraSaude;
import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.i9mind.agendador.business.repository.mongo.OperadoraSaudeMongoRepository;
import com.i9mind.agendador.business.repository.mongo.impl.mongodata.OperadoraSaudeSpringMongoRepository;
import com.i9mind.agendador.comum.exception.IntegrationException;


/**
 * 
 * DocumentBase
 * 
 * @author i9Mind - Victor Sarrasqueiro
 *
 */
@Repository
public class OperadoraSaudeMongoRepositoryImpl implements OperadoraSaudeMongoRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OperadoraSaudeMongoRepositoryImpl.class);

	@Autowired
	OperadoraSaudeSpringMongoRepository mongoRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;

	
	@Override
	public List<?> listar(DocumentBase pDocumentBase)  {
		
		try
		{
			Query query = new Query(Criteria.where("idEmp").is(pDocumentBase.getIdEmpresa()).and("ieAtv").is(true));
			query.fields().include("id").include("nmOpe");
			query.with(new Sort(Sort.Direction.ASC, "nmOpe"));
			
			return mongoTemplate.find(query, OperadoraSaude.class);
		}
		catch(Exception e)
		{
			LOGGER.error("OperadoraSaudeMongoRepositoryImpl - Erro ao listar" + e.getMessage());
			throw new IntegrationException("Erro ao listar.", e);
		}

	};
	
	@Override
	public DocumentBase obter(DocumentBase pDocumentBase) {
		
		try
		{
			Query query = new Query();
			query.addCriteria(Criteria.where("id").is(pDocumentBase.getId()).and("idEmp").is(pDocumentBase.getIdEmpresa()));
			
			return mongoTemplate.findOne(query, OperadoraSaude.class);
		}
		catch(Exception e)
		{
			LOGGER.error("OperadoraSaudeMongoRepositoryImpl - Erro ao obter" + e.getMessage());
			throw new IntegrationException("Erro ao obter.", e);
		}
		
	};
	
	@Override
	public DocumentBase inserir(DocumentBase pDocumentBase) {
		
		try
		{
			return mongoRepository.insert( (OperadoraSaude) pDocumentBase );
			
		}catch(Exception e)
		{
			LOGGER.error("OperadoraSaudeMongoRepositoryImpl - Erro ao inserir" + e.getMessage());
			throw new IntegrationException("Erro ao inserir.", e);
		}
		
	};
	
	@Override
	public void atualizar(DocumentBase pDocumentBase) {
		
		try
		{
			
			mongoRepository.save( (OperadoraSaude) pDocumentBase );
			
		}catch(Exception e)
		{
			LOGGER.error("OperadoraSaudeMongoRepositoryImpl - Erro ao atualizar" + e.getMessage());
			throw new IntegrationException("Erro ao atualizar.", e);
		}
	};
	
	@Override
	public void inativar(DocumentBase pDocumentBase) {
		
		try
		{
			pDocumentBase.setAtivo(false);
			mongoRepository.save( (OperadoraSaude) pDocumentBase );
		}
		catch(Exception e)
		{
			LOGGER.error("OperadoraSaudeMongoRepositoryImpl - Erro ao inativar" + e.getMessage());
			throw new IntegrationException("Erro ao inativar.", e);
		}
		
	}
	
	@Override
	public void ativar(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			pDocumentBase.setAtivo(true);
			mongoRepository.save( (OperadoraSaude) pDocumentBase );
		}
		catch(Exception e)
		{
			LOGGER.error("OperadoraSaudeMongoRepositoryImpl - Erro ao ativar" + e.getMessage());
			throw new IntegrationException("Erro ao ativar.", e);
		}
		
	}

	@Override
	public void excluir(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			mongoRepository.delete( (OperadoraSaude) pDocumentBase );
		}
		catch(Exception e)
		{
			LOGGER.error("OperadoraSaudeMongoRepositoryImpl - Erro ao excluir" + e.getMessage());
			throw new IntegrationException("Erro ao excluir.", e);
		}
		
	};

	@Override
	public List<?> listarInativo(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			Query query = new Query(Criteria.where("idEmp").is(pDocumentBase.getIdEmpresa()).and("ieAtv").is(false));
			query.fields().include("id").include("nmOpe");
			query.with(new Sort(Sort.Direction.ASC, "nmOpe"));
			
			return mongoTemplate.find(query, OperadoraSaude.class);
		}
		catch(Exception e)
		{
			LOGGER.error("OperadoraSaudeMongoRepositoryImpl - Erro ao listarInativo" + e.getMessage());
			throw new IntegrationException("Erro ao listarInativo.", e);
		}
		
	};

};
