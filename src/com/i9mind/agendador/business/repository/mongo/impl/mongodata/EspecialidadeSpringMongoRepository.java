package com.i9mind.agendador.business.repository.mongo.impl.mongodata;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.i9mind.agendador.business.entity.Especialidade;

@Repository
public interface EspecialidadeSpringMongoRepository extends MongoRepository<Especialidade, Long> {

}
