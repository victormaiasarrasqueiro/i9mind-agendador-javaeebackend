package com.i9mind.agendador.integration.service.rest.controller;


import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.i9mind.agendador.business.entity.Profissional;
import com.i9mind.agendador.business.entity.util.HttpResponse;

@RestController
@RequestMapping("/profissional")
public class ProfissionalRestController extends BaseRestController {
	
	private Profissional entity;

	@CrossOrigin(origins = "*")
    @RequestMapping(value = "/listar", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<HttpResponse> listar() {
		return super.listar(new Profissional());
	};
	
	@CrossOrigin(origins = "*")
    @RequestMapping(value = "/obter/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<HttpResponse> obter(@PathVariable String id) {
		entity = new Profissional();
		entity.setId(id);
		return super.obter(entity);
	};

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/inserir", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<HttpResponse> inserir(@Valid @RequestBody(required=true) Profissional pProfissional, BindingResult bindingResult) {
		return super.inserir(pProfissional, bindingResult);
	};
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/atualizar", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<HttpResponse> atualizar(@Valid @RequestBody(required=true) Profissional pProfissional, BindingResult bindingResult) {
		return super.atualizar(pProfissional, bindingResult);
	};
	
	@CrossOrigin(origins = "*")
    @RequestMapping(value = "/excluir/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<HttpResponse> excluir(@PathVariable String id) {
		entity = new Profissional();
		entity.setId(id);
		return super.excluir(entity);
	};

	@CrossOrigin(origins = "*")
    @RequestMapping(value = "/inativar/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<HttpResponse> inativar(@PathVariable String id) {
		entity = new Profissional();
		entity.setId(id);
		return super.inativar(entity);
	};
	
	@CrossOrigin(origins = "*")
    @RequestMapping(value = "/ativar/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<HttpResponse> ativar(@PathVariable String id) {
		entity = new Profissional();
		entity.setId(id);
		return super.ativar(entity);
	};
	
	@CrossOrigin(origins = "*")
    @RequestMapping(value = "/listarInativo", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<HttpResponse> listarInativo() {
		return super.listarInativo(new Profissional());
	};

};