package com.i9mind.agendador.comum.util.auxiliary;

import java.math.BigInteger;

public class NumberUtils extends ObjectUtils{
	
	
	public static boolean isNotEmpty(BigInteger number){
		
		if(isNotNull(number) && !number.toString().trim().equals("0")){
			return true;
		}else{
			return false;
		}
		
	}
	
	public static boolean isEmpty(BigInteger number){
		
		if(isNull(number) || number.toString().trim().equals("0")){
			return true;
		}else{
			return false;
		}
		
	}
	
	public static boolean isEmpty(Byte number){
		
		if(isNull(number) || number.toString().trim().equals("0")){
			return true;
		}else{
			return false;
		}
		
	}

}
