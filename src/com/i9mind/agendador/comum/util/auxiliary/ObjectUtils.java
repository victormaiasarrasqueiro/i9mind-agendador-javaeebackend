package com.i9mind.agendador.comum.util.auxiliary;

public class ObjectUtils {
	
	public static boolean isNull(Object o){
		return o == null;
	}
	
	public static boolean isNotNull(Object o){
		return o != null;
	}
}
