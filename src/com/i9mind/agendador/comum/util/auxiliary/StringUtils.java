package com.i9mind.agendador.comum.util.auxiliary;

public class StringUtils extends ObjectUtils{
	
	
	public static boolean isEmpty(String str){
		
		if(isNull(str) || str.trim().length() == 0){
			return true;
		}else{
			return false;
		}
		
	}
	
	public static boolean isNotEmpty(String str){
		
		return ! isEmpty(str);
	}
	
	public static boolean isMenorMaior(String str, int menor, int maior){
		
		if( isNotNull(str) && str.length() <  menor && str.length() > maior){
			return true;
		}else{
			return false;
		}
		
	}
	
	
}
