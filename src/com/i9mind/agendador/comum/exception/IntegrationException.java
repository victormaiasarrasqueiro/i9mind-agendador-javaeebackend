package com.i9mind.agendador.comum.exception;

public class IntegrationException extends RuntimeException {

	private static final long serialVersionUID = -4566649864936259416L;
	
	private String classe;
	private String metodo;
	private String mensagem;
	private String idUsuarioLogado;
	private String idEmpresaLogada;
	private Exception exception;
	

	public IntegrationException(String classe, String metodo, String mensagem, Exception exception) {
		super();
		this.classe = classe;
		this.metodo = metodo;
		this.mensagem = mensagem;
		this.exception = exception;
		this.idUsuarioLogado = "null";
		this.idEmpresaLogada = "null";
	}
	
	public IntegrationException(String classe, String metodo, String mensagem, String idUsuarioLogado,
			String idEmpresaLogada, Exception exception) {
		super();
		this.classe = classe;
		this.metodo = metodo;
		this.mensagem = mensagem;
		this.idUsuarioLogado = idUsuarioLogado;
		this.idEmpresaLogada = idEmpresaLogada;
		this.exception = exception;
	}

	public IntegrationException(String mensagem, Exception exception) {
		super();
		this.classe = "null";
		this.metodo = "null";
		this.mensagem = mensagem;
		this.exception = exception;
		this.idUsuarioLogado = "null";
		this.idEmpresaLogada = "null";
	}

	@Override
	public String toString() {
		return "IntegrationException [" + classe + "." + metodo + "  -   MSG => " + mensagem + "] exception: " +  exception;
	}


	public String getClasse() {
		return classe;
	}


	public void setClasse(String classe) {
		this.classe = classe;
	}


	public String getMetodo() {
		return metodo;
	}


	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}


	public String getMensagem() {
		return mensagem;
	}

	public String getIdUsuarioLogado() {
		return idUsuarioLogado;
	}

	public void setIdUsuarioLogado(String idUsuarioLogado) {
		this.idUsuarioLogado = idUsuarioLogado;
	}

	public String getIdEmpresaLogada() {
		return idEmpresaLogada;
	}

	public void setIdEmpresaLogada(String idEmpresaLogada) {
		this.idEmpresaLogada = idEmpresaLogada;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}


	public Exception getException() {
		return exception;
	}


	public void setException(Exception exception) {
		this.exception = exception;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
